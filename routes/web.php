<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminCategoryController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardPostController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//
// Route::get('/', function () {
//     return view('index');
// });

Route::get('/admin-category',[AdminCategoryController::class, 'index']);
Route::get('/category',[CategoryController::class, 'index']);
Route::get('/dashboard-post',[DashboardPostController::class, 'index']);
Route::get('/',[HomeController::class, 'index']);
Route::get('/login',[LoginController::class, 'index']);
Route::get('/post',[PostController::class, 'index']);
Route::get('/register',[RegisterController::class, 'index']);
Route::get('/user',[UsController::class, 'index']);
